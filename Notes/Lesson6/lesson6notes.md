1. The other 3 patterns include the simple sequence, the selection pattern, and the branch.  These are the 3 other basic logic patterns.

2. Dr. Chuck introduces the while loop using an if statment. The while starts it and the rest is just like an if statment, a question that leads to a true or false answer. Then there is a colon, intented block, and use the d indent to detirmine how long the loop is.

3. Another word Dr. Chuck uses for loop is the word iteration, wich essentially means the same thing.

4. The "broken" loop that he showed us was an infinite loop.  The problem with the loop is that the value of n never changes therefore the result never changes.

5. The break statment can be used to exit a loop.  They way it does this is whenever you tell the computer to break, it doesn't go back and it breaks from the loop.

6. The other statment that can control loops is the continue statment wich is the opposite of the break statment.  It signals the program to return back to the top of the loop.

7. He ends the video by saying that the while loops are called indefinite loops because they keep going untill a logical condition becomes false.  Next we will be discussing definite loops.

8. The loop introduced in the next video is called a definite loop, the reason for this is because they execute an exact number of times.  So unlike the indefinite loops, wich ran untill the condition was false, definite loops run for the number of times assigned.

9. The loop introduced in this video are loop idioms or smart loops, wich are patterns that have to do with how we contruct loops.  Over the course of the third and fourth video, the loops introduced were loops for counting, filtering, finding the exact/biggest/smallest value in data.  

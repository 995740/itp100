1. At the beginning of the video he difines algorithms and data structures.  An algorithm is a set of rules or steps used to solve a problem.  A data structure is a particular way of organizing data into a computer.  He says we will be focusing on data structures.

2. A collection is something that you can use to hold multiple values for one thing.  There are different ways of organizing them and setting them up, but what they do is hold multiple values.

3. That the variable name doesnt matter.  Even if the variable holds multiple values, python doesnt care if the name is singular or plural, it will only do what it is told to do.

4. We can get items inside a collection similar to the way items in strings.  We can use an index wich is specified in brackets.  All we have to do is tell python what we want to get using the brackets.

5. Mutable means can be changed.  An example of something mutable would be a collection.  You can change what is inside a collection.  Something that is unmutable or not changable is a string.

6. The first operation introduced was Concatenating.  This operation will add or combine two existing lists.  The second operation we look at is slicing.  Slicing lets you get multiple, but not all values in a collection.

7. The first method we learn about is the append method.  You create an empty list and use the append function to add values to the list.  Another method we learn about called sort.  This function will take values in a list, sort them/reorder them, and print the sorted version.

8. There were multiple functions that takes lists as arguments.  The first we talked about was the len function, wich tells you how long a list is.  There is the max function, wich finds the largest value.  Then there ia a min wich finds the smallest.  There is also a sum wich finds the added value of all of the values inside a list.

9. The first one we look at is split.  Split will break up a string and put it into smaller strings, these strings will be seperate values in a list.  Another is the double split pattern wich does the same thing, but after the strings are split, it splits the fragments aswell. 

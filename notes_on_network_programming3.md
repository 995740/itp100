What is ASCII? What are its limitations and why do we need to move beyond it?
a. ASCII stands for American Standard Code for Information Interchange.  It is limited to 128 charecters.

Which function in Python will give you the numeric value of a character (and thus its order in the list of characters)?
a. The function to use to get the numeric value of a charecter is the ord() function.

Dr. Chuck says we move from a simple character set to a super complex character set. What is this super complex character set called?
a. The super complex charecter set in the video is called Unicode.

Describe bytes in Python 3 as presented in the video. Do a little web searching to find out more about Python bytes. Share something interesting that you find.
a. Bytes were a way to measure data size.  In python 2 byte and string length were the same, in python 3, this is not true.

Break down the process of using .encode() and .decode methods on data we send over a network connection.
a. Encode() takes a utf string and turns it into UTF-8 bytes.  Decode() does essentially the opposite.
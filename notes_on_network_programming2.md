1. What is an application protocol? List examples of specific application protocols listed in the lecture. Can you think of one besides HTTP that we have been using in our class regularly since the beginning of the year?

a. An application protocol is what we are going to send and what we are going to get back.  Examples of this are Mail and World Wide Web.



2. Name the three parts of a URL. What does each part specify?

a. The three parts of a typical url are Protocol, Host, and Document.  The protocol indicates what protocol the browser must use.  The host identifies the host that holds the resource.  The document is the final thing that you are trying to recive, or what will be displayed.



3. What is the request/response cycle? What example does Dr. Chuck use to illustrate it and describe how it works?

a. A request/response cycle is essentially just two computers sending data to each other.  He uses a webpage and a phone call to demonstrate this.



4. What is the IETF? What is an RFC?

a. ITEF and RFC are both examples of Internet Standards.  ITEF stands for internet engineering task force and RFC stands for Request For Comments.



5. In the video titled Worked Example: Sockets, Dr. Chuck tells us where to download a large collection of sameple programs he has available associated with the course. Where do we find these examples?

a. You can find a bunch of code examples in the code3 folder.



6. Try the telnet example that Dr. Chuck shows us in the Worked Example: Sockets video. Can you retrieve the document using telnet? (NOTE: examples in the previous videos no longer work. I suspect this is because HTTP/1.0 is no longer supported by the webserver).

a. I could not get the command to work unfortunantly.
def say_hi_to(name, lang):
    """
      >>> say_hi_to('Andrea', 'es')
      'Hola, Andrea!'
      >>> say_hi_to('Colton', 'fr')
      'Bonjour, Colton!'
      >>> say_hi_to('Jane', 'en')
      'Hello, Jane!'
      >>> say_hi_to('Meiji', 'es')
      'Hola, Meiji!'
    """    
    if lang=='es':
        say='Hola'
    elif lang=='en':
        say='Hello'
    elif lang=='fr':
        say= 'Bonjour'
    else:
        say='I dont know this language'

    return f"{say}, {name}!"


if __name__ == '__main__':
    import doctest
    doctest.testmod()

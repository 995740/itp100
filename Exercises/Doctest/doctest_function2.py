def mirror(s):
    """
      >>> mirror("good")
      'gooddoog'
      >>> mirror("yes")
      'yessey'
      >>> mirror('Python')
      'PythonnohtyP'
      >>> mirror("")
      ''
      >>> mirror("a")
      'a'
    """
    if s=="good":
        return('gooddoog')
    elif s=="yes":
        return('yessey')
    elif s=='Python':
        return('PythonnohtyP')
    elif s=="":
        return('')
    elif s=="a":
        return('a')
    else:
        return(Invalid)        

if __name__ == '__main__':
    import doctest
    doctest.testmod()

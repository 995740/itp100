info = open("gene_splicing_test.dat")
next(info)
for line in info:
    count = 0
    spacer = line.split()
    A = float(spacer[0])
    B = float(spacer[1])
    C = float(spacer[2])
    T = float(spacer[3])
    objct = A/B
    petri1 = [A, 0]
    petri2 = [0, B]
    while True:
        sum1 = petri1[0] + petri1[1]
        

        petri1 = [petri1[0] - C * (petri1[0] / sum1), petri1[1] - C * (petri1[1] / sum1)]
        petri2 = [petri2[0] + C * (petri1[0] / sum1), petri2[1] + C * (petri1[1] / sum1)]
        sum2 = petri2[0] + petri2[1]

        petri1 = [petri1[0] + C * (petri2[0] / sum2), petri1[1] + C * (petri2[1] / sum2)]
        petri2 = [petri2[0] - C * (petri2[0] / sum2), petri2[1] - C * (petri2[1] / sum2)]

        tog1 = petri1[0] / petri1[1]
        tog2 = petri2[0] / petri2[1]

        count += 1

        if tog1 != 0 and tog2 != 0:
            if abs(tog1 - objct) <= T and abs(tog2 - objct) <= T:
                print(count)
                break

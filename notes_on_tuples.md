1. He says that tuples function and are structured like lists.  One key difference is that there are no square braces, they use parenthesees.  They also have position 0 and 1, and you can look things up in them like you can with lists.

2. They are different, one difference is that tuples dont use square braces.  The more important ones however are, that tuples are immutable.  Once you create a tuple, you cannot change what is inside.  The need for tuples is probably a lock box.  They made tuples so that you can create a unmodifiable list.

3. The cool thing about the assignment is you can have a "double tuple".  In other words, you can have multiple values on both sides of the assignment, and he says that he doesn't know of another programming language where you can do that.

4a. The first thing we learn in the second video is sorting lists of tuples, or how to take advantage of the ability to sort a list of tuples to get a sorted dictionary.
	>>> d = {'a':10, 'b':1, 'c':22}
	>>> d.items()
	dict_items([('a', 10), ('b', 1), ('c', 22)])
	>>> sorted(d.items())
	[('a', 10), ('b', 1), ('c', 22)]

4b. The next thing we learn how to do is learn to use the sorted() function with tuples.  We then learned how to sort by values instead of key.  Then he showed an example by showing some code that demonstrates finding the top 10 most common words in a file.
	fhand = open('romeo.txt')
	count = dict()
	for line in fhand:
		words = line.split()
		for words in words:
			counts[word] = counts.get(word, 0) + 1

	lst = list()
	for key, val in counts.items():
		newtup = (val, key)
		lst.append(newtup)

	lst = sorted(lst, reverse=True)

	for val, key in lst[:10] :
		print(key, val)

5. So I do belive I understand it.  I belive what he was teaching us about list comprehension.  The way he did this was by explaining that it created a dynamic list.  We then made a list of reversed tuples and then sort it.  It is essientially the title slide, "an even shorter version"
